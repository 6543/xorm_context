// Copyright 2022 The Gitea Authors. All rights reserved.
// SPDX-License-Identifier: MIT

package xorm_context

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"xorm.io/xorm"

	// blank imports to register the sql drivers
	_ "github.com/mattn/go-sqlite3"
)

func newTestEngine(t *testing.T) *xorm.Engine {
	engine, err := xorm.NewEngine("sqlite3", ":memory:")
	if !assert.NoError(t, err) {
		t.FailNow()
	}
	return engine
}

func TestInTransaction(t *testing.T) {
	SetDefaultEngine(context.Background(), newTestEngine(t))

	assert.False(t, InTransaction(defaultContext))
	assert.NoError(t, WithTx(defaultContext, func(ctx context.Context) error {
		assert.True(t, InTransaction(ctx))
		return nil
	}))

	ctx, committer, err := TxContext(defaultContext)
	assert.NoError(t, err)
	defer committer.Close()
	assert.True(t, InTransaction(ctx))
	assert.NoError(t, WithTx(ctx, func(ctx context.Context) error {
		assert.True(t, InTransaction(ctx))
		return nil
	}))
}

func TestTxContext(t *testing.T) {
	SetDefaultEngine(context.Background(), newTestEngine(t))

	{ // create new transaction
		ctx, committer, err := TxContext(defaultContext)
		assert.NoError(t, err)
		assert.True(t, InTransaction(ctx))
		assert.NoError(t, committer.Commit())
	}

	{ // reuse the transaction created by TxContext and commit it
		ctx, committer, err := TxContext(defaultContext)
		engine := GetEngine(ctx)
		assert.NoError(t, err)
		assert.True(t, InTransaction(ctx))
		{
			ctx, committer, err := TxContext(ctx)
			assert.NoError(t, err)
			assert.True(t, InTransaction(ctx))
			assert.Equal(t, engine, GetEngine(ctx))
			assert.NoError(t, committer.Commit())
		}
		assert.NoError(t, committer.Commit())
	}

	{ // reuse the transaction created by TxContext and close it
		ctx, committer, err := TxContext(defaultContext)
		engine := GetEngine(ctx)
		assert.NoError(t, err)
		assert.True(t, InTransaction(ctx))
		{
			ctx, committer, err := TxContext(ctx)
			assert.NoError(t, err)
			assert.True(t, InTransaction(ctx))
			assert.Equal(t, engine, GetEngine(ctx))
			assert.NoError(t, committer.Close())
		}
		assert.NoError(t, committer.Close())
	}

	{ // reuse the transaction created by WithTx
		assert.NoError(t, WithTx(defaultContext, func(ctx context.Context) error {
			assert.True(t, InTransaction(ctx))
			{
				ctx, committer, err := TxContext(ctx)
				assert.NoError(t, err)
				assert.True(t, InTransaction(ctx))
				assert.NoError(t, committer.Commit())
			}
			return nil
		}))
	}
}
